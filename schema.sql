drop table if exists entries;
drop table if exists sites;
drop table if exists logins;
create table entries (
  id integer primary key autoincrement,
  title text not null,
  text text not null);
create table sites (
  id integer primary key autoincrement,
  name text not null,
  location text,
  db text not null
);
create table logins (
  userid text primary key,
  pass text not null,
  name text not null);

INSERT INTO logins (userid, pass, name)
VALUES ('admin','default','Administrator');
