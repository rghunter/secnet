from setuptools import setup, find_packages


VERSION = "0.2"

setup(
    name="SecNet",
    description='Security and Authentication Server',
    author="Etiometry, Inc", author_email="info@etiometry.com",
    version=VERSION,
  #  packages = find_packages(),
    install_requires=['sqlalchemy', 'python-ldap', 'flask', 'flask-login', 'python-ldap']
)
