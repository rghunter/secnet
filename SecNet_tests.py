# -*- coding: utf-8 -*-
"""
    SecNet_server Tests
    ~~~~~~~~~~~~

    Tests the SecNet_server application.

    :copyright: (c) 2014 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
import os
import tempfile
import unittest

import SecNet_server


class SecNet_serverTestCase(unittest.TestCase):

    def setUp(self):
        """Before each test, set up a blank database"""
        self.db_fd, SecNet_server.app.config['DATABASE'] = tempfile.mkstemp()
        SecNet_server.app.config['TESTING'] = True
        self.app = SecNet_server.app.test_client()

    def tearDown(self):
        """Get rid of the database again after each test."""
        os.close(self.db_fd)
        os.unlink(SecNet_server.app.config['DATABASE'])

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    # testing functions
'''
    def test_login_logout(self):
        """Make sure login and logout works"""
        rv = self.login(SecNet_server.app.config['USERNAME'],
                        SecNet_server.app.config['PASSWORD'])
        assert b'You were logged in' in rv.data
        rv = self.logout()
        assert b'You were logged out' in rv.data
        rv = self.login(SecNet_server.app.config['USERNAME'] + 'x',
                        SecNet_server.app.config['PASSWORD'])
        assert b'Invalid username' in rv.data
        rv = self.login(SecNet_server.app.config['USERNAME'],
                        SecNet_server.app.config['PASSWORD'] + 'x')
        assert b'Invalid password' in rv.data
'''
if __name__ == '__main__':
    unittest.main()
