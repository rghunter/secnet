'''
Created on Jan 24, 2014

@author: rhunter
'''
import unittest

from SecNet import Identity


class IdentityTest(unittest.TestCase):

    def setUp(self):
        self.database = Identity.UserManager('sqlite:///:memory:')


    def tearDown(self):
        pass


    def testCreateUser(self):
        
        test_user = Identity.User('admin', 'administrator')
        self.database.add(test_user)
        test_pass = Identity.Password('admin', 'test_pass')
        self.database.add(test_pass)
        self.assertEqual(test_user, self.database.getUser('admin'))
        self.assertEqual(test_pass, self.database.getPassword('admin'))
    
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
