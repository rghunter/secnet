'''
Created on Feb 4, 2014

@author: rhunter
'''
import ldap

import helpers


class T3LDAP(object):
    '''
    classdocs
    '''
    def __init__(self, host, user, password, dc='T3', ou='Users'):
        '''
        Constructor
        Creates a binding for intereacting with T3 ldap entries. T3 Entries really aren't terribly different from normal
        LDAP entries, this just makes adding them a bit less confusing.
        @param host String: the host address of the server (do not inser ldap:// before hand
        @param user String: the user to use to commit and pull
        @param password String: the passowrd for that user
        '''
        self.dc = dc
        self.ou = ou
        self.host_string = "ldap://%s" % host
        self.user_dn = self.returnDN(user)
        self.ldap_con = ldap.initialize('ldap://ldap.spr.local')
        self.ldap_con.simple_bind_s(self.user_dn, password)
        
    def whoAmI(self):
        '''whoAmI
        quick function returning the login context info. Useful for testing
        '''
        return self.ldap_con.whoami_s()
    
    def returnDN(self, user, ou=None):
        if ou:
            return 'cn=%s,ou=%s,dc=%s' % (user, ou, self.dc)
        return 'cn=%s,dc=%s' % (user, self.dc)
    
    def getUser(self, user):
        '''Get User List
        Returns a user record
        '''
        return helpers.get_search_results(self.ldap_con.search_s('cn=%s,ou=%s,dc=%s' % (user, self.ou, self.dc), ldap.SCOPE_BASE, '(objectclass=*)', ['*'],))
        
    def close(self):
        '''Close
        Shutdown and ubind
        '''
        self.ldap_con.unbind()
        



        
        
