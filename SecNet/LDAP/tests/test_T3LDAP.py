'''
Created on Feb 4, 2014

@author: rhunter
'''
import unittest

from SecNet.LDAP import T3LDAP


class T3LDAPTest(unittest.TestCase):


    def setUp(self):
        self.test_con = T3LDAP.T3LDAP('ldap.spr.local', 'admin', 'Eti0l0gy', 'T3')
        pass


    def tearDown(self):
        self.test_con.close()
        pass

    def testReturnDN(self):
        self.assertEqual('cn=TEST,ou=USERS,dc=T3', self.test_con.returnDN('TEST', 'USERS'))
        self.assertEqual('cn=TEST,dc=T3', self.test_con.returnDN('TEST'))
        
    def testWhoAmI(self):
        self.assertEqual('dn:cn=admin,dc=T3', self.test_con.whoAmI())
        
    def testGetUser(self):
        for record in self.test_con.getUser('ch_national'):
            print record.pretty_print()

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
