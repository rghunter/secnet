'''
Created on Jan 24, 2014

@author: rhunter
'''

import flask_login
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker


Base = declarative_base()


class User(Base, flask_login.UserMixin):
    '''
    classdocs
    '''
    
    __tablename__ = 'users'
    userid = Column(String, primary_key=True)
    fullName = Column(String)
    password = relationship("Password", uselist=False, backref='users')

    def __init__(self, userid, name):
        self.userid = userid
        self.fullName = name 
        
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.userid == other.userid and self.fullName == other.fullName:
                return True
        else:
            return False
        
class Password(Base):
    '''
    classdocs
    '''
    
    __tablename__ = 'pass'
    userid = Column(String, ForeignKey('users.userid'), primary_key=True)
    password = Column(String)

    def __init__(self, userid, password):
        self.userid = userid
        self.password = password
        
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.userid == other.userid and self.password == other.password:
                return True
        else:
            return False

class UserManager(object):
    '''
    classdocs
    '''

    def __init__(self, Con_string, echo=True):
        '''
        Constructor
        @param location String the file location of the sqllite database
        '''
        self.engine = create_engine(Con_string, echo=echo)
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)
        self.cursor = self.session()
                
    def add(self, row):
        self.cursor.add(row)
        
    def getUser(self, userid):
        return self.cursor.query(User).filter_by(userid=userid).first()
    
    def getPassword(self, userid):
        return self.cursor.query(Password).filter_by(userid=userid).first()